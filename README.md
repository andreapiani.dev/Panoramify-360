Panoramify 360 - Creazione di tour interattivi

Questo software è libero e open source.
Per supportare il mio lavoro puoi farmi una donazione su https://www.buymeacoffee.com/andreapiani
oppure su https://coindrop.to/andreapiani

Puoi visitare il mio blog https://www.andreapiani.com

ENGLISH

FastPano 360 is a php script that lets you create awesome virtual tours for your customers without advanced programming knowledge. Using the built-in filemanager you can easily upload and manage your panoramic and thumbnail photos, with the virtual tour manager you can quickly add new scenes and publish it for your clients. FastPano 360 is simple to use. It has admin and client part. The admin part uses to manage virtual tours and the client part uses to view a virtual tour. All what you need is just to upload script files to your hosting, change the admin login and password and use it. This plugin is responsive and works on all modern browsers and mobile devices. Use this plugin to create interactive tours, maps and presentations.

The plugin's folder contains:

src - folder with source files (you should copy it to your web server)
application - folder with codeigniter application (config, controlers and views)
assets - folder with the main angularjs application source files and extra-libraries, css styles and images
lib - folder with the libraries used in the client part
system - system codeigniter folder
upload - folder for upload files (root)
vtours - folder for virtual tour config files
.htaccess - apache config for url routing
index.php - main php file

Uploading the script via FTP to your web server

Download the zipped script pack to your local computer from CodeCanyon and extract the ZIP file contents to a folder on your local computer.
In the extracted folder you will find the folder 'fastpano'.
Setup your settings in the config file 'fastpano/application/config/myconfig.php'. Set your own login and password.
Change settings in the config file 'fastpano/application/config/config.php'. You should set the right base_url (see codeigniter documentation).
Use an FTP client to access your host web server.
Upload the script folder 'fastpano' to your host web server (root directory or subdirectory).
Setup right permissions for upload and vtours folders.
Type in the browser http://yousite.com/fastpano (depends on the last step).
Check script working.

ITALIANO

FastPano 360 è uno script in php che ti permette di creare fantastici tour virtuali per i tuoi clienti senza conoscenze avanzate di programmazione. Usando il filemanager integrato puoi facilmente caricare e gestire le tue foto panoramiche e le miniature, con il virtual tour manager puoi aggiungere rapidamente nuove scene e pubblicarle per i tuoi clienti. FastPano 360 è semplice da usare. Ha una parte admin e una parte client. La parte admin usa per gestire i tour virtuali e la parte client usa per visualizzare un tour virtuale. Tutto ciò di cui hai bisogno è solo di caricare i file di script sul tuo hosting, cambiare il login e la password dell'amministratore e usarlo. Questo plugin è reattivo e funziona su tutti i moderni browser e dispositivi mobili. Usa questo plugin per creare tour interattivi, mappe e presentazioni.

La cartella del plugin contiene:

src - cartella con i file sorgente (dovresti copiarla sul tuo server web)
application - cartella con l'applicazione codeigniter (config, controlers e views)
assets - cartella con i file sorgente dell'applicazione angularjs principale e le librerie extra, gli stili css e le immagini
lib - cartella con le librerie utilizzate nella parte client
system - cartella codeigniter di sistema
upload - cartella per i file di upload (root)
vtours - cartella per i file di configurazione del tour virtuale
.htaccess - configurazione apache per il routing degli url
index.php - file php principale

Caricare lo script via FTP sul tuo server web

Scarica il pacchetto di script zippato sul tuo computer locale da CodeCanyon ed estrai il contenuto del file ZIP in una cartella sul tuo computer locale.
Nella cartella estratta troverai la cartella 'fastpano'.
Imposta le tue impostazioni nel file di configurazione 'fastpano/application/config/myconfig.php'. Imposta il tuo login e la tua password.
Cambia le impostazioni nel file di configurazione 'fastpano/application/config/config.php'. Dovresti impostare il giusto base_url (vedi documentazione codeigniter).
Usa un client FTP per accedere al tuo server web host.
Carica la cartella dello script 'fastpano' sul tuo server web (directory principale o sottodirectory).
Imposta i giusti permessi per le cartelle upload e vtours.
Scrivi nel browser http://yousite.com/fastpano (dipende dall'ultimo passo).
Controlla che lo script funzioni.
