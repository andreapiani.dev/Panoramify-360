<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="author" content="Max Lawrence">
	<meta name="copyright" content="Avirtum">
	<meta name="description" content="FastPano 360 is the PHP script that lets you create awesome virtual tours for your visitors without advanced programming knowledge">
	<title>FastPano 360 - Virtual Tour Constructor</title>
	
	<!-- css -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/font-awesome.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'assets/css/style.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'lib/tooltipster/css/tooltipster.bundle.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'lib/tooltipster/css/plugins/tooltipster/sidetip/themes/tooltipster-sidetip-borderless.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'lib/lightslider/lightslider.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'lib/jssocials/jssocials.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'lib/jssocials/jssocials-theme-plain-color.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'lib/ipanorama/ipanorama.min.css'; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'lib/ipanorama/themes/bubbles.min.css'; ?>">
	<!-- /end css -->
</head>
<body>
	<div class="fstpn-ui-page">
		<div class="fstpn-ui-panorama-wrap">
			<div class="fstpn-ui-panorama" id="panorama">
			</div>
			<?php if(sizeOf($scenes) > 1) { ?>
				<div id="panorama-slider-wrap" class="fstpn-ui-panorama-slider-wrap fstpn-ui-hidden">
					<div class="fstpn-ui-panorama-slider-toggle fstpn-ui-hidden"></div>
					<ul id="panorama-slider" class="fstpn-ui-panorama-slider">
						<?php foreach($scenes as $key => $scene) { ?>
							<?php $imageThumb = ($scene['imageThumb']['url'] ? $scene['imageThumb'] : $scene['image']); ?>
							<li>
								<div class="fstpn-ui-panorama-slider-thumb" data-scene="<?php echo $key; ?>">
									<img src="<?php echo ($imageThumb['isCustom'] ? $imageThumb['url'] : $uploadUrl . $imageThumb['url']); ?>" alt="">
								</div>
							</li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
			<div id="panorama-share-wrap" class="fstpn-ui-panorama-share-wrap">
				<div id="panorama-share" class="fstpn-ui-panorama-share">
				</div>
				<div id="panorama-embed" class="jssocials-share jssocials-share-embed">
					<a href="#" class="jssocials-share-link">
						<i class="fa fa-code jssocials-share-logo"></i>
						<span class="jssocials-share-label">Embed</span>
					</a>
				</div>
				<br/>
				<input id="panorama-embed-code" class="fstpn-ui-panorama-embed-code" value="<iframe width='560' height='315' src='<?php echo current_url() ?>' allowfullscreen frameborder='0' scrolling='no'></iframe>">
			</div>
		</div>
	</div>
	
	<!-- scripts -->
	<script src="<?php echo base_url() . 'assets/js/lib/jquery.min.js'; ?>"></script>
	<script src="<?php echo base_url() . 'assets/js/view.js'; ?>"></script>
	<!-- libraries -->
	<script src="<?php echo base_url() . 'lib/tooltipster/js/tooltipster.bundle.min.js'; ?>"></script>
	<script src="<?php echo base_url() . 'lib/lightslider/lightslider.min.js'; ?>"></script>
	<script src="<?php echo base_url() . 'lib/jssocials/jssocials.min.js'; ?>"></script>
	<script src="<?php echo base_url() . 'lib/ipanorama/three.min.js'; ?>"></script>
	<script src="<?php echo base_url() . 'lib/ipanorama/ipanorama.min.js'; ?>"></script>
	<script src="<?php echo base_url() . 'lib/ipanorama/jquery.ipanorama.min.js'; ?>"></script>
	<!-- /end scripts -->
	
	<!-- panorama script -->
	<script type="text/javascript">
		<?php
			function IsNotNullOrEmpty($input){
				return !(!isset($input) || trim($input)==='');
			}
		?>
		jQuery(document).ready(function(jQuery) {
			jQuery('#panorama').ipanorama({
				theme: 'default',
				widget: {name: null},
				autoLoad: true, // load a first scene after init
				scenes: [
					<?php foreach($scenes as $key => $scene) { ?>
						{
							title : '<?php echo ($scene['title']); ?>',
							type: 'sphere',
							image: '<?php echo ($scene['image']['isCustom'] ? $scene['image']['url'] : $uploadUrl . $scene['image']['url']); ?>',
							imageThumb: null,
							camera: {
								pos: {
									alpha: <?php echo (IsNotNullOrEmpty($scene['yaw']) ? $scene['yaw'] : 0); ?>,
									beta: <?php echo (IsNotNullOrEmpty($scene['pitch']) ? $scene['pitch'] : 0); ?>
								},
								zoom : <?php echo (IsNotNullOrEmpty($scene['zoom']) && $scene['zoom'] >= 0 && $scene['zoom'] <= 75 ? $scene['zoom'] : 75); ?>
							},
							autoRotate: {
								enabled: false, // automatically rotate when the panorama is loaded
								inactivityDelay: 3000, // sets the delay, in milliseconds, to start automatically rotating the panorama after user activity ceases, this parameter only has an effect if the autoRotate parameter is set
								speedX: 0.5,
								speedY: 0,
								speedZ: 0
							},
						},
					<?php } ?>
				],
				onLoad: function() {
					var instance = this;
					
					// attach the slider otherwise the fullscreen mode doesn't have the slider
					var $el = $('#panorama-slider-wrap').detach();
					instance.$container.append($el);
					
					// slider initialization
					$('#panorama-slider').lightSlider({
						autoWidth:true,
						loop:false,
						pager:false,
						gallery:true,
						onSliderLoad: function() {
							$('.fstpn-ui-panorama-slider-wrap').removeClass('fstpn-ui-hidden');
							$('.fstpn-ui-panorama-slider-toggle').removeClass('fstpn-ui-hidden');
						}
					});
					
					// slider button off/on
					$('.fstpn-ui-panorama-slider-toggle').on('click', function() {
						var $sliderWrap = $('.fstpn-ui-panorama-slider-wrap');
						$sliderWrap.toggleClass('fstpn-ui-hidden');
					});
					
					// slider thumb click event
					$('.fstpn-ui-panorama-slider-thumb').on('click', function() {
						var sceneId = $(this).data('scene');
						sceneId = parseInt(sceneId, 10);
						sceneId = instance.config.scenes[sceneId].id;
						
						instance.setScene({id:sceneId});
					});
					
					// social share initialization
					var $el = $('#panorama-share-wrap').detach();
					instance.$container.append($el);
					
					$('#panorama-share').jsSocials({
						text: 'FastPano 360 is the PHP script that lets you create awesome virtual tours.',
						showLabel:true,
						showCount:true,
						shareIn:'popup',
						shares: ['email', 'twitter', 'facebook', 'googleplus', 'stumbleupon']
					});
					
					var $el = $('#panorama-embed').detach();
					$el.on('click', function() {
						$('#panorama-embed-code').toggleClass('fstpn-ui-active');
					});
					$('#panorama-share .jssocials-shares').append($el);
					
					// title
					var $title = $('<div>').addClass('fstpn-ui-title');
					instance.$container.append($title);
					
					instance.dom.$canvas.on('ipanorama:scene-show-start', function(e) {
						var data = e.originalEvent.data;
						$title.text(data.scene.cfg.title ? data.scene.cfg.title : '');
					});
					
					// progress
					var $progress = $('<div>').addClass('fstpn-ui-progress');
					instance.$container.append($progress);
					
					instance.dom.$canvas.on('ipanorama:scene-progress', function(e) {
						var data = e.originalEvent.data;
						$progress.addClass('fstpn-ui-active');
					});
					instance.dom.$canvas.on('ipanorama:scene-after-load', function(e) {
						var data = e.originalEvent.data;
						$progress.removeClass('fstpn-ui-active');
						
						for(var i=0;i<instance.config.scenes.length;i++) {
							var scene = instance.config.scenes[i];
							if(scene.id == data.scene.cfg.id) {
								$('#panorama-slider').find('.fstpn-ui-panorama-slider-thumb').removeClass('fstpn-ui-active');
								$('#panorama-slider').find('.fstpn-ui-panorama-slider-thumb[data-scene="' + i + '"]').addClass('fstpn-ui-active');
								break;
							}
						}
					});
					
					// prev & next
					var $prev = $('<div>').addClass('fstpn-ui-prev'),
					$next = $('<div>').addClass('fstpn-ui-next');
					instance.$container.append($prev, $next);
					
					$prev.on('click', function(e) {
						if(!(instance.viewer && instance.viewer.sceneActive)) return;
						
						var sceneId = instance.viewer.sceneActive.cfg.id;
						for(var i=0;i<instance.config.scenes.length;i++) {
							var scene = instance.config.scenes[i];
							if(scene.id == sceneId) {
								break;
							}
						}
						
						if(i-1<0) i = instance.config.scenes.length-1;
						else i = i-1;
						
						instance.setScene({id: instance.config.scenes[i].id});
					});
					
					$next.on('click', function(e) {
						if(!(instance.viewer && instance.viewer.sceneActive)) return;
						
						var sceneId = instance.viewer.sceneActive.cfg.id;
						for(var i=0;i<instance.config.scenes.length;i++) {
							var scene = instance.config.scenes[i];
							if(scene.id == sceneId) {
								break;
							}
						}
						
						if(i+1>=instance.config.scenes.length) i = 0;
						else i = i+1;
						
						instance.setScene({id: instance.config.scenes[i].id});
					});
					
					// zoom in & out
					var $zoomOut = $('<div>').addClass('fstpn-ui-zoomout'),
					$zoomIn = $('<div>').addClass('fstpn-ui-zoomin');
					instance.$container.append($zoomOut, $zoomIn);
					
					var timerId = null,
					zoomIn = function() {
						timerId = setTimeout(function() {
							var control = instance.viewer.sceneActive.control,
							zoom = control.getZoom();
							
							zoom = zoom + 0.2;
							
							control.setZoom(zoom);
							
							zoomIn();
						},10);
					},
					zoomOut = function() {
						timerId = setTimeout(function() {
							var control = instance.viewer.sceneActive.control,
							zoom = control.getZoom();
							
							zoom = zoom - 0.2;
							
							control.setZoom(zoom);
							
							zoomOut();
						},10);
					};
					
					$zoomIn.on('mousedown touchstart', function() {
						clearTimeout(timerId);
						zoomIn();
					});
					
					$zoomOut.on('mousedown touchstart', function() {
						clearTimeout(timerId);
						zoomOut();
					});
					
					instance.$container.on('mouseup touchend', function() {
						clearTimeout(timerId);
					});
					
					// fullscreen
					var $fullscreen = $('<div>').addClass('fstpn-ui-fullscreen');
					instance.$container.append($fullscreen);
					
					$fullscreen.on('click', function(e) {
						if(!$fullscreen.hasClass('fstpn-ui-active')) {
							try {
								var el = instance.$container.get(0);
								if(el.requestFullscreen) { el.requestFullscreen(); }
								else if(el.mozRequestFullScreen) { el.mozRequestFullScreen(); } 
								else if(el.webkitRequestFullscreen) { el.webkitRequestFullscreen(); } 
								else if(el.msRequestFullscreen) { el.msRequestFullscreen(); }
							} catch(ex) {} // fullscreen doesn't work
						} else {
							try {
								if(document.exitFullscreen) { document.exitFullscreen(); }
								else if(document.mozCancelFullScreen) { document.mozCancelFullScreen(); }
								else if(document.webkitExitFullscreen) { document.webkitExitFullscreen(); }
								else if(document.msExitFullscreen) { document.msExitFullscreen(); }
							} catch(ex) {}
						}
						return false;
					});
					
					instance.$container.on('fullscreenchange mozfullscreenchange webkitfullscreenchange msfullscreenchange', function(e) {
						if (document.fullscreen || document.mozFullScreen || document.webkitIsFullScreen || document.msFullscreenElement) {
							$fullscreen.addClass('fstpn-ui-active');
						} else {
							$fullscreen.removeClass('fstpn-ui-active');
						}
						
						setTimeout(function() {
							instance._onWindowResize();
						}, 100);
					});
					
					// share
					var $share = $('<div>').addClass('fstpn-ui-share');
					instance.$container.append($share);
					
					$share.on('click', function() {
						$('#panorama-share-wrap').toggleClass('fstpn-ui-active');
					});
				}
			});
		});
	</script>
	<!-- /end custom script -->
</body>
</html>